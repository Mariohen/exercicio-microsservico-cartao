package br.com.mastertech.cartao.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE",configuration = ClienteCartaoConfiguracao.class)
public interface ClienteCartao {

    @GetMapping("/cliente/{id}")
    Cliente getById(@PathVariable Integer id);

}

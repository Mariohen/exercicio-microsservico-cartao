package br.com.mastertech.cartao.cliente;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteCartaoConfiguracao {

    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new ClienteDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClienteCartaoFallback(), RetryableException.class)
                .withFallbackFactory(ClienteCartaoLoadBalanceConfiguracao::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}

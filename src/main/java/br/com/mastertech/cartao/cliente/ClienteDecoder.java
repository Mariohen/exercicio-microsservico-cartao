package br.com.mastertech.cartao.cliente;
import br.com.mastertech.cartao.services.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

import feign.codec.ErrorDecoder;

public class ClienteDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClienteNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }



}

package br.com.mastertech.cartao.cliente;

import br.com.mastertech.cartao.services.ClienteNotFoundException;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteCartaoLoadBalanceConfiguracao implements ClienteCartao {

    private Exception exception;

    public ClienteCartaoLoadBalanceConfiguracao(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Cliente getById(Integer id) {
        if(exception.getCause() instanceof ClienteNotFoundException) {
            Cliente cliente = new Cliente();
            return cliente;
        }
        throw (RuntimeException) exception;
    }
}

package br.com.mastertech.cartao.services;

import br.com.mastertech.cartao.DTOs.CartaoPesquisaSemStatusDTO;
import br.com.mastertech.cartao.DTOs.CartaoSaidaDTO;
import br.com.mastertech.cartao.DTOs.CartaoStatusDTO;
import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartaoService {


    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao salvarCartao(Cartao cartao) {
        Cartao objetoCartao = cartaoRepository.save(cartao);
        return cartao;
    }

    public Cartao buscarCartaoPeloId(int id) {
        Cartao cartao = cartaoRepository.findFirstById(id);

        if (cartao != null) {
            return cartao;
        } else {
            throw new CartaoNotFoundException();
        }
    }

    public Cartao buscarCartaoPeloNumero(String numero) {
        Cartao cartao = cartaoRepository.findFirstByNumero(numero);

        if (cartao != null) {
            return cartao;
        } else {
            throw new RuntimeException("O cartao não foi encontrado");
        }
    }

    public CartaoSaidaDTO alterarStatusAtivo(String numero, CartaoStatusDTO cartaoStatusDTO){
        Cartao cartao = cartaoRepository.findFirstByNumero(numero);
        if(cartao != null){
            cartao.setAtivo(cartaoStatusDTO.getAtivo());
            cartao = cartaoRepository.save(cartao);

            return new CartaoSaidaDTO(cartao);
        }
        else
        {
            throw new RuntimeException("Número do cartão não existe");
        }
    }

    public CartaoPesquisaSemStatusDTO buscarPorNumero(String numero){

        Cartao cartao = cartaoRepository.findFirstByNumero(numero);
        if(cartao != null){
            return new CartaoPesquisaSemStatusDTO(cartao);
        }
        else {
            throw new RuntimeException("Número de cartão não existe");
        }
    }




}

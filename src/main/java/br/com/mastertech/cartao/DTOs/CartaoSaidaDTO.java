package br.com.mastertech.cartao.DTOs;

import br.com.mastertech.cartao.model.Cartao;

public class CartaoSaidaDTO {
        private Integer id;

        private String numero;

        private Integer clienteId;

        private Boolean ativo;

        public Integer getId() {
        return id;
    }

        public void setId(Integer id) {
        this.id = id;
    }

        public String getNumero() {
        return numero;
    }

        public void setNumero(String numero) {
        this.numero = numero;
    }

        public Integer getClienteId() {
        return clienteId;
    }

        public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

        public Boolean getAtivo() {
        return ativo;
    }

        public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoSaidaDTO() {
    }

    public CartaoSaidaDTO(Cartao cartao)
        {
            this.setId(cartao.getId());
            this.setNumero(cartao.getNumero());
            this.setClienteId(cartao.getClienteId());
            this.setAtivo(true);
        }

    }

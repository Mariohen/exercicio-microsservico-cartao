package br.com.mastertech.cartao.controller;

import br.com.mastertech.cartao.DTOs.CartaoPesquisaSemStatusDTO;
import br.com.mastertech.cartao.DTOs.CartaoSaidaDTO;
import br.com.mastertech.cartao.DTOs.CartaoStatusDTO;
import br.com.mastertech.cartao.cliente.Cliente;
import br.com.mastertech.cartao.cliente.ClienteCartao;
import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.services.CartaoService;
import br.com.mastertech.cartao.services.ClienteNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoControler {

    @Autowired
    private ClienteCartao clienteCartao;

    @Autowired
    private CartaoService cartaoService;

//    @PostMapping
//    public C create(@PathVariable String name, @PathVariable String plate) {
//        Person person = new Person();
//        person.setName(name);
//
//        Cliente byPlate = clienteCartao.getByPlate(plate);
//        person.setCarModel(byPlate.getModel());
//
//        return person;
//    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao cadastrarCartao(@RequestBody @Valid Cartao cartao) {
         clienteCartao.getById(cartao.getClienteId());
         Cartao objetoCartao = cartaoService.salvarCartao(cartao);
         return objetoCartao;

    }

    @GetMapping("/{id}")
    public Cartao buscarPorNumero(@PathVariable Integer id){
            return cartaoService.buscarCartaoPeloId(id);
    }


    @PatchMapping("/{numero}")
    public CartaoSaidaDTO alterarStatusAtivo(@PathVariable(name = "numero") String numero, @RequestBody @Valid CartaoStatusDTO cartaoStatusDTO)
    {
            return cartaoService.alterarStatusAtivo(numero, cartaoStatusDTO);

    }



}

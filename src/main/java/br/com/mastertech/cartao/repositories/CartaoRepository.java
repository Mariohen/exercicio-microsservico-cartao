package br.com.mastertech.cartao.repositories;

import br.com.mastertech.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    Cartao findFirstByNumero(String numero);
    Cartao findFirstById(int id);

}

